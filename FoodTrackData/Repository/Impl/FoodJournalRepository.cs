﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using FoodTrackBlazorData.Contexts;
using FoodTrackBlazorData.Models;
using FoodTrackBlazorData.Repository.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace FoodTrackBlazorData.Repository.Impl
{
    public class FoodJournalRepository : BaseRepository<FoodJournal>, IFoodJournalRepository
    {
        public FoodJournalRepository(FoodTrackContext foodTrackContext) : base(foodTrackContext)
        {
        }
    }
}
