﻿using FoodTrackBlazorData.Contexts;
using FoodTrackBlazorData.Models;
using FoodTrackBlazorData.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackBlazorData.Repository.Impl
{
    public class CalendarEventRepository : BaseRepository<CalendarEvent>, ICalendarEventRepository
    {
        private readonly IdentityContext identityContext;

        public CalendarEventRepository(FoodTrackContext foodTrackContext, IdentityContext identityContext) : base(foodTrackContext)
        {
            this.foodTrackContext = foodTrackContext;
            this.identityContext = identityContext;
        }
    }
}
