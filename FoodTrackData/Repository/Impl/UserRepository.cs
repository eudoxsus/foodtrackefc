﻿using FoodTrackBlazorData.Contexts;
using FoodTrackBlazorData.Repository.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackBlazorData.Repository.Impl
{
    public class UserRepository : IUserRepository
    {

        private readonly IdentityContext identityContext;

        public UserRepository(IdentityContext identityContext)
        {
            this.identityContext = identityContext;
        }

        public async Task<IdentityUser> GetUserByIDAsync(string id)
        {
            return await identityContext.FindAsync<IdentityUser>(id);
        }

        public async Task<IdentityUser> GetUserByNormNameAsync(string normalizedName)
        {
            return await identityContext.Users.SingleAsync(x => x.NormalizedUserName == normalizedName);
        }
    }
}
