﻿using System;
using System.Collections.Generic;

using System.Text;
using FoodTrackBlazorData.Models;
using System.Threading.Tasks;
using FoodTrackBlazorData.Contexts;
using System.Linq;
using System.Linq.Expressions;
using FoodTrackBlazorData.Repository.Interfaces;

namespace FoodTrackBlazorData.Repository.Impl
{
    public class CarbohydrateRepository : BaseRepository<Carbohydrate>, ICarbohyrdateRepository
    {

        public CarbohydrateRepository(FoodTrackContext foodTrackContext) : base(foodTrackContext)
        {
            this.foodTrackContext = foodTrackContext;
        }
    }
}
