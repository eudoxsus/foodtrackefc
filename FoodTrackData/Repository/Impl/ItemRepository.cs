﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using FoodTrackBlazorData.Contexts;
using FoodTrackBlazorData.Models;
using FoodTrackBlazorData.Repository.Interfaces;

namespace FoodTrackBlazorData.Repository.Impl
{
    public class ItemRepository : BaseRepository<Item>, IItemRepository
    {
       
        public ItemRepository(FoodTrackContext foodTrackContext) : base(foodTrackContext)
        {
            
        }
    }
}
