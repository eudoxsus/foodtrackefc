﻿using FoodTrackBlazorData.Contexts;
using FoodTrackBlazorData.Models;
using FoodTrackBlazorData.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackBlazorData.Repository.Impl
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {

        protected FoodTrackContext foodTrackContext;

        public BaseRepository(FoodTrackContext foodTrackContext)
        {
            this.foodTrackContext = foodTrackContext;
        }

        public void SetEntryModified(T entry)
        {
            foodTrackContext.Entry(entry).State = EntityState.Modified;
        }

        public virtual async Task SaveAsync()
        {
            await foodTrackContext.SaveChangesAsync();
        }

        public virtual async Task<T> CreateAsync(T entity)
        {
            await foodTrackContext.AddAsync(entity);
            await foodTrackContext.SaveChangesAsync();
            return entity;
        }

        public virtual async Task<T> GetAsync(int id)
        {
            return await foodTrackContext.FindAsync<T>(id);
        }

        public virtual async Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> filter = null, Func<System.Linq.IQueryable<T>, System.Linq.IOrderedQueryable<T>> orderBy = null, string includeProperties = "")
        {
            IQueryable<T> query = foodTrackContext.Set<T>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includeProperties != null)
            {
                foreach (var includeProp in includeProperties
                    .Split(new char[] { ',' },StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProp);
                }
            }

            if (orderBy != null)
            {
                return await orderBy(query).ToListAsync();
            }
            else
            {
                return await query.ToListAsync();
            }

        }
    }
}
