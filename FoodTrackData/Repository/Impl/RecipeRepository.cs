﻿using FoodTrackBlazorData.Contexts;
using FoodTrackBlazorData.Models;
using FoodTrackBlazorData.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackBlazorData.Repository.Impl
{
    public class RecipeRepository : BaseRepository<Recipe>, IRecipeRepository
    {
        public RecipeRepository(FoodTrackContext foodTrackContext) : base(foodTrackContext)
        {

        }
    }
}
