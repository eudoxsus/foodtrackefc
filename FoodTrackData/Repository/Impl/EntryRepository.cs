﻿using System;
using System.Collections.Generic;

using System.Text;
using FoodTrackBlazorData.Models;
using System.Threading.Tasks;
using FoodTrackBlazorData.Contexts;
using System.Linq;
using System.Linq.Expressions;
using FoodTrackBlazorData.Repository.Interfaces;

namespace FoodTrackBlazorData.Repository.Impl
{
    public class EntryRepository : BaseRepository<Entry>, IEntryRepository
    {

        public EntryRepository(FoodTrackContext foodTrackContext) : base(foodTrackContext)
        {
            
        }

        public Task<Entry> CreateAsync(Entry entity)
        {
            throw new NotImplementedException();
        }

        public async Task<Entry> CreateEntryAsync(Entry entry)
        {
            await foodTrackContext.Entries.AddAsync(entry);
            await foodTrackContext.SaveChangesAsync();
            return entry;
        }

        public Task<Entry> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Entry>> GetAsync(Expression<Func<Entry, bool>> filter = null, Func<IQueryable<Entry>, IOrderedQueryable<Entry>> orderBy = null, string includeProperties = "")
        {
            throw new NotImplementedException();
        }

        public async Task<Entry> GetEntryAsync(int id)
        {
            return await foodTrackContext.Entries.FindAsync(id);
        }
    }
}
