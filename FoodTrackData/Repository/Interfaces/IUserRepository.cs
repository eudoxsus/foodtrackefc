﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackBlazorData.Repository.Interfaces
{
    public interface IUserRepository
    {
        Task<IdentityUser> GetUserByIDAsync(string id);
        Task<IdentityUser> GetUserByNormNameAsync(string normalizedName);
    }
}
