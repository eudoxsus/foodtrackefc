﻿using FoodTrackBlazorData.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackBlazorData.Repository.Interfaces
{
    public interface ICalendarEventRepository : IBaseRepository<CalendarEvent>
    {
    }
}
