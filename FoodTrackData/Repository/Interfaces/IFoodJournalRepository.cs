﻿using FoodTrackBlazorData.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackBlazorData.Repository.Interfaces
{
    public interface IFoodJournalRepository : IBaseRepository<FoodJournal>
    {
    }
}
