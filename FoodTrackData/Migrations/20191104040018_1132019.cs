﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodTrackBlazorData.Migrations
{
    public partial class _1132019 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_FoodJournals_CreatingUserName",
                table: "FoodJournals");

            migrationBuilder.DropColumn(
                name: "CreatingUserName",
                table: "Recipes");

            migrationBuilder.DropColumn(
                name: "CreatingUserName",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "CreatingUserName",
                table: "ItemRecipes");

            migrationBuilder.DropColumn(
                name: "CreatingUserName",
                table: "FoodJournals");

            migrationBuilder.DropColumn(
                name: "CreatingUserName",
                table: "Fats");

            migrationBuilder.DropColumn(
                name: "CreatingUserName",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "Date",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "CreatingUserName",
                table: "Carbohydrates");

            migrationBuilder.DropColumn(
                name: "CreatingUserName",
                table: "CalendarEvents");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Recipes",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Recipes",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<string>(
                name: "CreatingUserNameNormalized",
                table: "Recipes",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Items",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Items",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<string>(
                name: "CreatingUserNameNormalized",
                table: "Items",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "ItemRecipes",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "ItemRecipes",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<string>(
                name: "CreatingUserNameNormalized",
                table: "ItemRecipes",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "FoodJournals",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "FoodJournals",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<string>(
                name: "CreatingUserNameNormalized",
                table: "FoodJournals",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Fats",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Fats",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<string>(
                name: "CreatingUserNameNormalized",
                table: "Fats",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Entries",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Entries",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<string>(
                name: "CreatingUserNameNormalized",
                table: "Entries",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Carbohydrates",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Carbohydrates",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<string>(
                name: "CreatingUserNameNormalized",
                table: "Carbohydrates",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "CalendarEvents",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "CalendarEvents",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<string>(
                name: "CreatingUserNameNormalized",
                table: "CalendarEvents",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FoodJournals_CreatingUserNameNormalized",
                table: "FoodJournals",
                column: "CreatingUserNameNormalized");

            migrationBuilder.CreateIndex(
                name: "IX_Entries_CreatedDate",
                table: "Entries",
                column: "CreatedDate");

            migrationBuilder.CreateIndex(
                name: "IX_Entries_CreatingUserNameNormalized",
                table: "Entries",
                column: "CreatingUserNameNormalized");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_FoodJournals_CreatingUserNameNormalized",
                table: "FoodJournals");

            migrationBuilder.DropIndex(
                name: "IX_Entries_CreatedDate",
                table: "Entries");

            migrationBuilder.DropIndex(
                name: "IX_Entries_CreatingUserNameNormalized",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "CreatingUserNameNormalized",
                table: "Recipes");

            migrationBuilder.DropColumn(
                name: "CreatingUserNameNormalized",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "CreatingUserNameNormalized",
                table: "ItemRecipes");

            migrationBuilder.DropColumn(
                name: "CreatingUserNameNormalized",
                table: "FoodJournals");

            migrationBuilder.DropColumn(
                name: "CreatingUserNameNormalized",
                table: "Fats");

            migrationBuilder.DropColumn(
                name: "CreatingUserNameNormalized",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "CreatingUserNameNormalized",
                table: "Carbohydrates");

            migrationBuilder.DropColumn(
                name: "CreatingUserNameNormalized",
                table: "CalendarEvents");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Recipes",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Recipes",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.AddColumn<string>(
                name: "CreatingUserName",
                table: "Recipes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Items",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Items",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.AddColumn<string>(
                name: "CreatingUserName",
                table: "Items",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "ItemRecipes",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "ItemRecipes",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.AddColumn<string>(
                name: "CreatingUserName",
                table: "ItemRecipes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "FoodJournals",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "FoodJournals",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.AddColumn<string>(
                name: "CreatingUserName",
                table: "FoodJournals",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Fats",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Fats",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.AddColumn<string>(
                name: "CreatingUserName",
                table: "Fats",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Entries",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Entries",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.AddColumn<string>(
                name: "CreatingUserName",
                table: "Entries",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "Entries",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Carbohydrates",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Carbohydrates",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.AddColumn<string>(
                name: "CreatingUserName",
                table: "Carbohydrates",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "CalendarEvents",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "CalendarEvents",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.AddColumn<string>(
                name: "CreatingUserName",
                table: "CalendarEvents",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FoodJournals_CreatingUserName",
                table: "FoodJournals",
                column: "CreatingUserName");
        }
    }
}
