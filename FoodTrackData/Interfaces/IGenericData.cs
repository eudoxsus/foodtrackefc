﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace FoodTrackEFC.Shared.Interfaces
{
    public interface IGenericData
    {
        IReadOnlyDictionary<string, object> DataDict {get;set;}
    }
}
