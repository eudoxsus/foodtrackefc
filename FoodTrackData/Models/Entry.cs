﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FoodTrackBlazorData.Models
{
    public class Entry : BaseEntity
    {
        public int ID { get; set; }
        public Item Item { get; set; }
        // The multiple consumed of this food
        public int QuantityMultiplier;
        public FoodJournal FoodJournal;
    }
}
