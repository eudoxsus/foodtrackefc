﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FoodTrackBlazorData.Models
{
    /// <summary>
    /// A FoodJournal consists of a collection of FoodEntries associated with a certain date and User
    /// </summary>
    public class FoodJournal : BaseEntity
    {
        public int ID { get; set; }
        public CalendarEvent CalendarEvent { get; set; }
        public virtual ICollection<Entry> Entries {get; set;}
        public virtual ICollection<Recipe> Recipes { get; set; }
    }
}
