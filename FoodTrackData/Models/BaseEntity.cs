﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FoodTrackBlazorData.Models
{
    public abstract class BaseEntity
    {
        public string CreatingUserNameNormalized { get; set; }
        [Column(TypeName = "date")]
        public DateTime CreatedDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime UpdatedDate { get; set; }
    }
}
