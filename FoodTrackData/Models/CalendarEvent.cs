﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FoodTrackBlazorData.Models
{
    public class CalendarEvent : BaseEntity
    {
        public int ID { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Color { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public bool IsFullDay { get; set; }
        public virtual ICollection<FoodJournal> FoodJournals { get; set; }
    }
}
