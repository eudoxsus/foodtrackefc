﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodTrackBlazorData.Models
{
    public class Recipe : BaseEntity
    {
        public int ID { get; set; }
        public virtual ICollection<ItemRecipe> ItemRecipes { get; set; }
    }
}
