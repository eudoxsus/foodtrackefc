﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodTrackBlazorData
{
    public interface IIdentifiable
    {
        int ID { get; set; }
    }
}
