﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using System.Text;
using FoodTrackEFC.Shared.Annotations;

namespace FoodTrackEFC.Client.Pages
{
    public class Test
    {
        public string Email { get; set; }

        class Obj
        {
            [NotDisplayed]
            public int prop1 { get; set; } = 1;
            public int prop2 { get; set; } = 2;
        }

        public void Main()
        {
            Console.WriteLine("Hello World");

            Obj formObject = new Obj();

            foreach (PropertyInfo rootProp in formObject.GetType().GetProperties())
            {



                PropertyInfo propInfo = rootProp;
                Type propT = propInfo.PropertyType;

                rootProp.SetValue(formObject, 10);



            }
        }

        public void foo()
        {
            StringBuilder stringBuilder;
        }
    }
}
