﻿
//using FoodTrackBlazorData.Contexts;
using FoodTrackEFC.Shared.Interfaces;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodTrackEFC.Client.InterOp
{
    public class InterOpHelper : IInterOpHelper
    {
        private readonly IJSRuntime jSRuntime;

        public InterOpHelper(IJSRuntime jSRuntime)
        {
            this.jSRuntime = jSRuntime;
        }

        [JSInvokable]
        public string TestInter(string test)
        {
            return test;
        }

        /// <summary>
        /// Invoked by JavaScript to Create an instance of this class for JavaScript to use
        /// </summary>
        /// <returns></returns>
        public ValueTask<object> CreateJSRef()
        {
            return jSRuntime.InvokeAsync<object>(
                "jsInterOpScripts.createJSInterOpRef",
                DotNetObjectReference.Create(this));
        }
    }
}
