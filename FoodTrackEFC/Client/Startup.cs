using Blazored.LocalStorage;
using FoodTrackBlazorData.Repository.Impl;
using FoodTrackBlazorData.Repository.Interfaces;
using FoodTrackEFC.Client.InterOp;
using FoodTrackEFC.Client.Services;
using FoodTrackEFC.Shared.Interfaces;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace FoodTrackEFC.Client
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddBlazoredLocalStorage();
            services.AddAuthorizationCore();
            services.AddScoped<AuthenticationStateProvider, ApiAuthenticationStateProvider>();
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IInterOpHelper, InterOpHelper>();
            services.AddScoped<ICalendarEventRepository, CalendarEventRepository>();
        }

        public void Configure(IComponentsApplicationBuilder app)
        {
            app.AddComponent<App>("app");
        }
    }
}
