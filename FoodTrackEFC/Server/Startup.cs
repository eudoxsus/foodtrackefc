
using FoodTrackBlazorData.Contexts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Components.Authorization;
using Newtonsoft.Json.Serialization;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Configuration;
using FoodTrackEFC.Shared.Interfaces;
using FoodTrackEFC.Client.InterOp;
using FoodTrackEFC.Shared.Mappers;
using FoodTrackEFC.Shared.Managers;
using FoodTrackBlazorData.Repository.Interfaces;
using FoodTrackBlazorData.Repository.Impl;
using System.Text.Json.Serialization;

namespace FoodTrackEFC.Server
{
    public class Startup
    {

        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().AddJsonOptions(options => {
                options.JsonSerializerOptions.ReferenceHandling = ReferenceHandling.Preserve;
            } );
            services.AddScoped<IDataMapper, DataMapper>();

            services.AddScoped<IFatManager, FatManager>();
            services.AddScoped<ICarbohydrateManager, CarbohydrateManager>();
            services.AddScoped<IItemManager, ItemManager>();
            
            services.AddScoped<ICalendarEventRepository, CalendarEventRepository>();
            services.AddScoped<ICarbohyrdateRepository, CarbohydrateRepository>();
            services.AddScoped<IEntryRepository, EntryRepository>();
            services.AddScoped<IFatRepository, FatRepository>();
            services.AddScoped<IFoodJournalRepository, FoodJournalRepository>();
            services.AddScoped<IItemRepository, ItemRepository>();
            services.AddScoped<IRecipeRepository, RecipeRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddResponseCompression(opts =>
            {
                opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
                    new[] { "application/octet-stream" });
            });

            services.AddDbContext<IdentityContext>(
                options => options.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=AuthenticationWithClientSideBlazor;Trusted_Connection=True;MultipleActiveResultSets=true"));

            services.AddDefaultIdentity<IdentityUser>()
                .AddEntityFrameworkStores<IdentityContext>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = "https://localhost",
                    ValidAudience = "https://localhost",
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtSecurityKey"]))
                };
            });

            services.AddDbContext<FoodTrackContext>(
                options => options.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=FoodTrackDb;Trusted_Connection=True;MultipleActiveResultSets=true"));

            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseResponseCompression();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBlazorDebugging();
            }

            app.UseStaticFiles();
            app.UseClientSideBlazorFiles<Client.Startup>();

            app.UseRouting();
            //app.UseAuthentication();
            //app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
                endpoints.MapFallbackToClientSideBlazor<Client.Startup>("index.html");
            });
        }
    }
}
