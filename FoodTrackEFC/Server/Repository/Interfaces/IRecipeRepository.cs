﻿using FoodTrackBlazorData.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackEFC.Shared.Repository.Interfaces
{
    public interface IRecipeRepository
    {
        Task<Recipe> GetRecipeAsync(int id);
        Task<Recipe> CreateRecipeAsync(Recipe recipe);
    }
}
