﻿using FoodTrackBlazorData.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackEFC.Shared.Repository.Interfaces
{
    public interface IItemRepository
    {
        Task<Item> GetItemAsync(int id);
        Task<Item> CreateItemAsync(Item item);
    }
}
