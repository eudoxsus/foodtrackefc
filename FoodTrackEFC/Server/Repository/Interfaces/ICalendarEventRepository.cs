﻿using FoodTrackBlazorData.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackEFC.Shared.Repository.Interfaces
{
    public interface ICalendarEventRepository
    {
        Task<CalendarEvent> GetCalendarEventAsync(int id);
        Task<CalendarEvent> CreateCalendarEventAsync(CalendarEvent calendarEvent);
    }
}
