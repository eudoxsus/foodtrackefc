﻿using FoodTrackBlazorData.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackEFC.Shared.Repository.Interfaces
{
    public interface IFoodJournalRepository
    {
        Task<FoodJournal> GetFoodJournalAsync(int id);
        Task<FoodJournal> CreateFoodJournalAsync(FoodJournal foodJournal,IdentityUser user);
        Task<FoodJournal> CreateFoodJournalAsync(FoodJournal foodJournal,string userId);
    }
}
