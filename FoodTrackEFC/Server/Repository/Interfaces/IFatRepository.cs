﻿using FoodTrackBlazorData.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackEFC.Shared.Repository.Interfaces
{
    public interface IFatRepository
    {
        Task<Fat> GetFatAsync(int id);
        Task<Fat> CreateFatAsync(Fat Fat);
    }
}
