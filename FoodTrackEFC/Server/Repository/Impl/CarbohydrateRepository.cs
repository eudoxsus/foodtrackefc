﻿using System;
using System.Collections.Generic;
using FoodTrackEFC.Shared.Repository.Interfaces;
using System.Text;
using FoodTrackBlazorData.Models;
using System.Threading.Tasks;
using FoodTrackBlazorData.Contexts;

namespace FoodTrackBlazorData.Shared.Repository.Impl
{
    public class CarbohydrateRepository : ICarbohyrdateRepository
    {
        private readonly FoodTrackContext foodTrackContext;

        public CarbohydrateRepository(FoodTrackContext foodTrackContext)
        {
            this.foodTrackContext = foodTrackContext;
        }

        public Task<Carbohydrate> CreateCarbohydrateAsync(Carbohydrate carbohydrate)
        {
            throw new NotImplementedException();
        }

        public Task<Carbohydrate> GetCarbohydrateAsync(int carbId)
        {
            throw new NotImplementedException();
        }
    }
}
