﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FoodTrackBlazorData.Contexts;
using FoodTrackBlazorData.Models;
using FoodTrackEFC.Shared.Repository.Interfaces;

namespace FoodTrackBlazorData.Shared.Repository.Impl
{
    public class ItemRepository : IItemRepository
    {
        private readonly FoodTrackContext foodTrackContext;

        public ItemRepository(FoodTrackContext foodTrackContext)
        {
            this.foodTrackContext = foodTrackContext;
        }


        public Task<Item> CreateItemAsync(Item item)
        {
            throw new NotImplementedException();
        }

        public Task<Item> GetItemAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
