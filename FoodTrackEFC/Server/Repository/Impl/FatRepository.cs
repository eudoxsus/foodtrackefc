﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FoodTrackBlazorData.Contexts;
using FoodTrackBlazorData.Models;
using FoodTrackEFC.Shared.Repository.Interfaces;

namespace FoodTrackBlazorData.Shared.Repository.Impl
{
    public class FatRepository : IFatRepository
    {
        private readonly FoodTrackContext foodTrackContext;

        public FatRepository(FoodTrackContext foodTrackContext)
        {
            this.foodTrackContext = foodTrackContext;
        }

        public async Task<Fat> CreateFatAsync(Fat Fat)
        {
            await foodTrackContext.Fats.AddAsync(Fat);
            await foodTrackContext.SaveChangesAsync();
            return Fat;
        }

        public async Task<Fat> GetFatAsync(int id)
        {
            return await foodTrackContext.Fats.FindAsync(id);
        }
    }
}
