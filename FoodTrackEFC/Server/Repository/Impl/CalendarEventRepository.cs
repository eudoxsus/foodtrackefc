﻿using FoodTrackBlazorData.Contexts;
using FoodTrackBlazorData.Models;
using FoodTrackEFC.Shared.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackBlazorData.Shared.Repository.Impl
{
    public class CalendarEventRepository : ICalendarEventRepository
    {
        private readonly FoodTrackContext foodTrackContext;
        private readonly IdentityContext identityContext;

        public CalendarEventRepository(FoodTrackContext foodTrackContext, IdentityContext identityContext)
        {
            this.foodTrackContext = foodTrackContext;
            this.identityContext = identityContext;
        }

        public Task<CalendarEvent> CreateCalendarEventAsync(CalendarEvent calendarEvent)
        {
            throw new NotImplementedException();
        }

        public Task<CalendarEvent> GetCalendarEventAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
