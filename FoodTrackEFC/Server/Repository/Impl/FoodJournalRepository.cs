﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FoodTrackBlazorData.Contexts;
using FoodTrackBlazorData.Models;
using FoodTrackEFC.Shared.Repository.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace FoodTrackBlazorData.Shared.Repository.Impl
{
    public class FoodJournalRepository : IFoodJournalRepository
    {
        private readonly FoodTrackContext foodTrackContext;
        private readonly IdentityContext identityContext;

        public FoodJournalRepository(FoodTrackContext foodTrackContext, IdentityContext identityContext)
        {
            this.foodTrackContext = foodTrackContext;
            this.identityContext = identityContext;
        }

        public async Task<FoodJournal> CreateFoodJournalAsync(FoodJournal foodJournal, IdentityUser user)
        {
            foodJournal.CreatingUserID = user.Id;
            await foodTrackContext.FoodJournals.AddAsync(foodJournal);
            return foodJournal;
        }

        public async Task<FoodJournal> CreateFoodJournalAsync(FoodJournal foodJournal, string userId)
        {
            foodJournal.CreatingUserID = userId;
            await foodTrackContext.FoodJournals.AddAsync(foodJournal);
            return foodJournal;
        }

        public Task<FoodJournal> GetFoodJournalAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
