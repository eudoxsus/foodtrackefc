﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodTrackEFC.Shared;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FoodTrackEFC.Server.Controllers
{
    [Route("api/[controller]")]
    public class AccountsController : ControllerBase
    {

        private static UserModel LoggedOutUser = new UserModel { IsAuthenticated = false };

        private readonly UserManager<IdentityUser> _userManger;

        public AccountsController(UserManager<IdentityUser> userManager)
        {
            _userManger = userManager;
        }

        // POST api/<controller>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]RegisterModel model)
        {
            var newUser = new IdentityUser { Email = model.Email, UserName = model.Email };

            var res = await _userManger.CreateAsync(newUser, model.Password);

            if (!res.Succeeded)
            {
                var errors = res.Errors.Select(x => x.Description);

                return BadRequest(new RegisterResult { Successful = false, Errors = errors });
            }
            return Ok(new RegisterResult { Successful = true });
        }
    }
}
