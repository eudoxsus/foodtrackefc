﻿using FoodTrackEFC.Shared.Dtos;
using FoodTrackEFC.Shared.Managers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodTrackEFC.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FoodJournalController : ControllerBase
    {
        private IItemManager ItemManager { get; }

        public FoodJournalController(IItemManager itemManager)
        {
            this.ItemManager = itemManager;
        }

        [HttpGet]
        public async Task<IEnumerable<ItemDto>> GetItems()
        {
            return await ItemManager.GetAsync();
        }

        [HttpGet("{id}")]
        public async Task<ItemDto> GetItem(int id)
        {
            return await ItemManager.GetAsync(id);
        }


        [HttpPost]
        public async Task<ItemDto> CreateItem([FromBody] ItemDto itemDto)
        {
            return await ItemManager.CreateAsync(itemDto);
        }
    }
}
