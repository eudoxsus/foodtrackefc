﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodTrackBlazorData;
using FoodTrackBlazorData.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FoodTrackBlazorData.Contexts;
using FoodTrackEFC.Shared.Dtos;
using FoodTrackBlazorData.Repository.Interfaces;
using FoodTrackEFC.Shared.Managers;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Authorization;
using System.Text.Json;
using FoodTrackEFC.Client.Static;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FoodTrackEFC.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemController : ControllerBase
    {
        private IItemManager ItemManager { get; }

        public ItemController(IItemManager itemManager)
        {
            this.ItemManager = itemManager;
        }

        [HttpGet]
        public async Task<IEnumerable<ItemDto>> GetItems()
        {
            return await ItemManager.GetAsync();
        }

        [HttpGet("{id}")]
        public async Task<ItemDto> GetItem(int id)
        {
            return await ItemManager.GetAsync(id);
        }

        
        // Make a Generic Searching Api?
        [HttpGet("{searchTerm}")]
        public async Task<IEnumerable<ItemDto>> SearchItemsByName(string searchText)
        {
            //this.User.Identity.
            //filter
            Expression<Func<FoodTrackBlazorData.Models.Item, bool>> filterById = x => x.Name.Contains(searchText, StringComparison.OrdinalIgnoreCase);

            Task<IEnumerable<ItemDto>> itemFilterByName = ItemManager.GetAsync(filter: filterById);
            return await itemFilterByName;
        }


        [HttpPost]
        public async Task<ItemDto> CreateItem([FromBody] string itemDtoStr)
        {
            ItemDto deserialized = JsonSerializer.Deserialize<ItemDto>(itemDtoStr, CustomHttpClientJsonExtensions.JsonSerializerOptionsProvider.Options);
            ItemDto created = await ItemManager.CreateAsync(deserialized);
            return created;
        }

        [HttpPut("{id}")]
        public async Task<ItemDto> UpdateItem(int id,[FromBody] ItemDto itemDto)
        {
            return await ItemManager.UpdateAsync(id,itemDto);
        }
    }
}
