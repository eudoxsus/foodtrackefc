﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodTrackBlazorData.Contexts;
using FoodTrackBlazorData.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FoodTrackEFC.Server.Controllers
{
    [Route("api/[controller]")]
    public class CarbohydrateController : ControllerBase
    {

        FoodTrackContext trackContext;

        public CarbohydrateController(FoodTrackContext foodTrackContext)
        {
            this.trackContext = foodTrackContext;
        }



        [HttpGet]
        public async Task<IEnumerable<Carbohydrate>> GetUsers()
        {
            return await trackContext.Carbohydrates.ToListAsync();
        }


        [HttpPost]
        public async Task<Carbohydrate> CreateUser(Carbohydrate carbohydrate)
        {
            trackContext.Carbohydrates.Add(carbohydrate);
            await trackContext.SaveChangesAsync();
            return carbohydrate;
        }
    }
}
