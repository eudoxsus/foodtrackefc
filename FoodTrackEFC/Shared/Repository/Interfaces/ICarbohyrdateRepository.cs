﻿using FoodTrackBlazorData.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackEFC.Shared.Repository.Interfaces
{
    public interface ICarbohyrdateRepository : IBaseRepository<Carbohydrate>
    {
        
    }
}
