﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using FoodTrackBlazorData.Contexts;
using FoodTrackBlazorData.Models;
using FoodTrackEFC.Shared.Repository.Interfaces;

namespace FoodTrackBlazorData.Shared.Repository.Impl
{
    public class ItemRepository : IItemRepository
    {
        private readonly FoodTrackContext foodTrackContext;

        public ItemRepository(FoodTrackContext foodTrackContext)
        {
            this.foodTrackContext = foodTrackContext;
        }

        public Task<Item> CreateAsync(Item entity)
        {
            throw new NotImplementedException();
        }

        public Task<Item> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Item>> GetAsync(Expression<Func<Item, bool>> filter = null, Func<IQueryable<Item>, IOrderedQueryable<Item>> orderBy = null, string includeProperties = "")
        {
            throw new NotImplementedException();
        }
    }
}
