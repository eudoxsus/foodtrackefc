﻿using System;
using System.Collections.Generic;
using FoodTrackEFC.Shared.Repository.Interfaces;
using System.Text;
using FoodTrackBlazorData.Models;
using System.Threading.Tasks;
using FoodTrackBlazorData.Contexts;

namespace FoodTrackBlazorData.Shared.Repository.Impl
{
    public class EntryRepository : IEntryRepository
    {
        private readonly FoodTrackContext foodTrackContext;

        public EntryRepository(FoodTrackContext foodTrackContext)
        {
            this.foodTrackContext = foodTrackContext;
        }

        public async Task<Entry> CreateEntryAsync(Entry entry)
        {
            await foodTrackContext.Entries.AddAsync(entry);
            await foodTrackContext.SaveChangesAsync();
            return entry;
        }

        public async Task<Entry> GetEntryAsync(int id)
        {
            return await foodTrackContext.Entries.FindAsync(id);
        }
    }
}
