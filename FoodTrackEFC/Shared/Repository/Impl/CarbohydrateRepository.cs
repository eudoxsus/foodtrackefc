﻿using System;
using System.Collections.Generic;
using FoodTrackEFC.Shared.Repository.Interfaces;
using System.Text;
using FoodTrackBlazorData.Models;
using System.Threading.Tasks;
using FoodTrackBlazorData.Contexts;
using System.Linq;
using System.Linq.Expressions;

namespace FoodTrackBlazorData.Shared.Repository.Impl
{
    public class CarbohydrateRepository : ICarbohyrdateRepository
    {
        private readonly FoodTrackContext foodTrackContext;

        public CarbohydrateRepository(FoodTrackContext foodTrackContext)
        {
            this.foodTrackContext = foodTrackContext;
        }

        public Task<Carbohydrate> CreateAsync(Carbohydrate entity)
        {
            throw new NotImplementedException();
        }

        public Task<Carbohydrate> CreateCarbohydrateAsync(Carbohydrate carbohydrate)
        {
            throw new NotImplementedException();
        }

        public Task<Carbohydrate> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Carbohydrate>> GetAsync(Expression<Func<Carbohydrate, bool>> filter = null, Func<IQueryable<Carbohydrate>, IOrderedQueryable<Carbohydrate>> orderBy = null, string includeProperties = "")
        {
            throw new NotImplementedException();
        }

        public Task<Carbohydrate> GetCarbohydrateAsync(int carbId)
        {
            throw new NotImplementedException();
        }
    }
}
