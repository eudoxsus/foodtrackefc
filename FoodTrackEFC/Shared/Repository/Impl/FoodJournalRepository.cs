﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using FoodTrackBlazorData.Contexts;
using FoodTrackBlazorData.Models;
using FoodTrackEFC.Shared.Repository.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace FoodTrackBlazorData.Shared.Repository.Impl
{
    public class FoodJournalRepository : IFoodJournalRepository
    {
        private readonly FoodTrackContext foodTrackContext;
        private readonly IdentityContext identityContext;

        public FoodJournalRepository(FoodTrackContext foodTrackContext, IdentityContext identityContext)
        {
            this.foodTrackContext = foodTrackContext;
            this.identityContext = identityContext;
        }

        public Task<FoodJournal> CreateAsync(FoodJournal entity)
        {
            throw new NotImplementedException();
        }

        public async Task<FoodJournal> CreateFoodJournalAsync(FoodJournal foodJournal, IdentityUser user)
        {
            foodJournal.UserID = user.Id;
            await foodTrackContext.FoodJournals.AddAsync(foodJournal);
            return foodJournal;
        }

        public async Task<FoodJournal> CreateFoodJournalAsync(FoodJournal foodJournal, string userId)
        {
            foodJournal.UserID = userId;
            await foodTrackContext.FoodJournals.AddAsync(foodJournal);
            return foodJournal;
        }

        public Task<FoodJournal> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<FoodJournal>> GetAsync(Expression<Func<FoodJournal, bool>> filter = null, Func<IQueryable<FoodJournal>, IOrderedQueryable<FoodJournal>> orderBy = null, string includeProperties = "")
        {
            throw new NotImplementedException();
        }

        public Task<FoodJournal> GetFoodJournalAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
