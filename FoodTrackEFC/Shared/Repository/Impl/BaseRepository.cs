﻿using FoodTrackBlazorData.Models;
using FoodTrackEFC.Shared.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodTrackEFC.Shared.Repository.Impl
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        public System.Threading.Tasks.Task<T> CreateAsync(T entity)
        {
            throw new NotImplementedException();
        }

        public System.Threading.Tasks.Task<T> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public System.Threading.Tasks.Task<IEnumerable<T>> GetAsync(System.Linq.Expressions.Expression<Func<T, bool>> filter = null, Func<System.Linq.IQueryable<T>, System.Linq.IOrderedQueryable<T>> orderBy = null, string includeProperties = "")
        {
            throw new NotImplementedException();
        }
    }
}
