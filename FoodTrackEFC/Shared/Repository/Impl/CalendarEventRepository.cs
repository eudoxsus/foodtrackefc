﻿using FoodTrackBlazorData.Contexts;
using FoodTrackBlazorData.Models;
using FoodTrackEFC.Shared.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackBlazorData.Shared.Repository.Impl
{
    public class CalendarEventRepository : ICalendarEventRepository
    {
        private readonly FoodTrackContext foodTrackContext;
        private readonly IdentityContext identityContext;

        public CalendarEventRepository(FoodTrackContext foodTrackContext, IdentityContext identityContext)
        {
            this.foodTrackContext = foodTrackContext;
            this.identityContext = identityContext;
        }

        public Task<CalendarEvent> CreateAsync(CalendarEvent entity)
        {
            throw new NotImplementedException();
        }

        public Task<CalendarEvent> CreateCalendarEventAsync(CalendarEvent calendarEvent)
        {
            throw new NotImplementedException();
        }

        public Task<CalendarEvent> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<CalendarEvent>> GetAsync(Expression<Func<CalendarEvent, bool>> filter = null, Func<IQueryable<CalendarEvent>, IOrderedQueryable<CalendarEvent>> orderBy = null, string includeProperties = "")
        {
            throw new NotImplementedException();
        }

        public Task<CalendarEvent> GetCalendarEventAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
