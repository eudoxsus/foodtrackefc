﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using FoodTrackBlazorData.Contexts;
using FoodTrackBlazorData.Models;
using FoodTrackEFC.Shared.Repository.Interfaces;

namespace FoodTrackBlazorData.Shared.Repository.Impl
{
    public class FatRepository : IFatRepository
    {
        private readonly FoodTrackContext foodTrackContext;

        public FatRepository(FoodTrackContext foodTrackContext)
        {
            this.foodTrackContext = foodTrackContext;
        }

        public Task<Fat> CreateAsync(Fat entity)
        {
            throw new NotImplementedException();
        }

        public async Task<Fat> CreateFatAsync(Fat Fat)
        {
            await foodTrackContext.Fats.AddAsync(Fat);
            await foodTrackContext.SaveChangesAsync();
            return Fat;
        }

        public Task<Fat> GetAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Fat>> GetAsync(Expression<Func<Fat, bool>> filter = null, Func<IQueryable<Fat>, IOrderedQueryable<Fat>> orderBy = null, string includeProperties = "")
        {
            throw new NotImplementedException();
        }

        public async Task<Fat> GetFatAsync(int id)
        {
            return await foodTrackContext.Fats.FindAsync(id);
        }
    }
}
