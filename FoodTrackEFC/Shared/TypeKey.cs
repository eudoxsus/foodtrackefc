﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodTrackEFC.Shared
{
    /// <summary>
    /// Custom Dictionary Key that uses Type Class for comparison
    /// </summary>
    public class TypeKey
    {

        public Type Type { get; private set; }

        public TypeKey(Type type)
        {
            this.Type = type;
        }

        public class EqualityComparer : IEqualityComparer<TypeKey>
        {
            public bool Equals(TypeKey x, TypeKey y)
            {
                return x.Type.Equals(y.Type);
            }

            public int GetHashCode(TypeKey key)
            {
                return key.Type.FullName.GetHashCode();
            }
        }
    }
}
