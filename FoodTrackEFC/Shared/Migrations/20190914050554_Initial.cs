﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodTrackBlazorData.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CalendarEvents",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Start = table.Column<DateTime>(nullable: false),
                    End = table.Column<DateTime>(nullable: false),
                    Color = table.Column<string>(nullable: true),
                    Subject = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    IsFullDay = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CalendarEvents", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Fat",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Unsaturated = table.Column<int>(nullable: false),
                    Saturated = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fat", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "FoodJournals",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FoodJournals", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Entries",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FoodJournalID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Entries", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Entries_FoodJournals_FoodJournalID",
                        column: x => x.FoodJournalID,
                        principalTable: "FoodJournals",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Recipes",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FoodJournalID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Recipes", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Recipes_FoodJournals_FoodJournalID",
                        column: x => x.FoodJournalID,
                        principalTable: "FoodJournals",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Source = table.Column<string>(nullable: true),
                    Protein = table.Column<int>(nullable: false),
                    Calories = table.Column<int>(nullable: false),
                    FatID = table.Column<int>(nullable: true),
                    Unit = table.Column<string>(nullable: true),
                    BaseSrvSize = table.Column<float>(nullable: false),
                    EntryID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Items_Entries_EntryID",
                        column: x => x.EntryID,
                        principalTable: "Entries",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Items_Fat_FatID",
                        column: x => x.FatID,
                        principalTable: "Fat",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Carbohydrates",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ItemID = table.Column<int>(nullable: false),
                    Total = table.Column<int>(nullable: false),
                    Net = table.Column<int>(nullable: false),
                    Fiber = table.Column<int>(nullable: false),
                    Sugar = table.Column<int>(nullable: false),
                    AddedSugar = table.Column<int>(nullable: false),
                    Unit = table.Column<string>(nullable: true),
                    EntryID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Carbohydrates", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Carbohydrates_Entries_EntryID",
                        column: x => x.EntryID,
                        principalTable: "Entries",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Carbohydrates_Items_ItemID",
                        column: x => x.ItemID,
                        principalTable: "Items",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ItemRecipes",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RecipeID = table.Column<int>(nullable: false),
                    ItemID = table.Column<int>(nullable: true),
                    ItemSize = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemRecipes", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ItemRecipes_Items_ItemID",
                        column: x => x.ItemID,
                        principalTable: "Items",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ItemRecipes_Recipes_RecipeID",
                        column: x => x.RecipeID,
                        principalTable: "Recipes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Carbohydrates_EntryID",
                table: "Carbohydrates",
                column: "EntryID");

            migrationBuilder.CreateIndex(
                name: "IX_Carbohydrates_ItemID",
                table: "Carbohydrates",
                column: "ItemID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Entries_FoodJournalID",
                table: "Entries",
                column: "FoodJournalID");

            migrationBuilder.CreateIndex(
                name: "IX_ItemRecipes_ItemID",
                table: "ItemRecipes",
                column: "ItemID");

            migrationBuilder.CreateIndex(
                name: "IX_ItemRecipes_RecipeID",
                table: "ItemRecipes",
                column: "RecipeID");

            migrationBuilder.CreateIndex(
                name: "IX_Items_EntryID",
                table: "Items",
                column: "EntryID");

            migrationBuilder.CreateIndex(
                name: "IX_Items_FatID",
                table: "Items",
                column: "FatID");

            migrationBuilder.CreateIndex(
                name: "IX_Recipes_FoodJournalID",
                table: "Recipes",
                column: "FoodJournalID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CalendarEvents");

            migrationBuilder.DropTable(
                name: "Carbohydrates");

            migrationBuilder.DropTable(
                name: "ItemRecipes");

            migrationBuilder.DropTable(
                name: "Items");

            migrationBuilder.DropTable(
                name: "Recipes");

            migrationBuilder.DropTable(
                name: "Entries");

            migrationBuilder.DropTable(
                name: "Fat");

            migrationBuilder.DropTable(
                name: "FoodJournals");
        }
    }
}
