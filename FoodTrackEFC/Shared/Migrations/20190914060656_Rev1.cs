﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodTrackBlazorData.Migrations
{
    public partial class Rev1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_Entries_EntryID",
                table: "Items");

            migrationBuilder.DropIndex(
                name: "IX_Items_EntryID",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "EntryID",
                table: "Items");

            migrationBuilder.AddColumn<int>(
                name: "CalendarEventID",
                table: "FoodJournals",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EntryID",
                table: "Fat",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ItemID",
                table: "Entries",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FoodJournals_CalendarEventID",
                table: "FoodJournals",
                column: "CalendarEventID");

            migrationBuilder.CreateIndex(
                name: "IX_Fat_EntryID",
                table: "Fat",
                column: "EntryID");

            migrationBuilder.CreateIndex(
                name: "IX_Entries_ItemID",
                table: "Entries",
                column: "ItemID");

            migrationBuilder.AddForeignKey(
                name: "FK_Entries_Items_ItemID",
                table: "Entries",
                column: "ItemID",
                principalTable: "Items",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Fat_Entries_EntryID",
                table: "Fat",
                column: "EntryID",
                principalTable: "Entries",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FoodJournals_CalendarEvents_CalendarEventID",
                table: "FoodJournals",
                column: "CalendarEventID",
                principalTable: "CalendarEvents",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Entries_Items_ItemID",
                table: "Entries");

            migrationBuilder.DropForeignKey(
                name: "FK_Fat_Entries_EntryID",
                table: "Fat");

            migrationBuilder.DropForeignKey(
                name: "FK_FoodJournals_CalendarEvents_CalendarEventID",
                table: "FoodJournals");

            migrationBuilder.DropIndex(
                name: "IX_FoodJournals_CalendarEventID",
                table: "FoodJournals");

            migrationBuilder.DropIndex(
                name: "IX_Fat_EntryID",
                table: "Fat");

            migrationBuilder.DropIndex(
                name: "IX_Entries_ItemID",
                table: "Entries");

            migrationBuilder.DropColumn(
                name: "CalendarEventID",
                table: "FoodJournals");

            migrationBuilder.DropColumn(
                name: "EntryID",
                table: "Fat");

            migrationBuilder.DropColumn(
                name: "ItemID",
                table: "Entries");

            migrationBuilder.AddColumn<int>(
                name: "EntryID",
                table: "Items",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Items_EntryID",
                table: "Items",
                column: "EntryID");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_Entries_EntryID",
                table: "Items",
                column: "EntryID",
                principalTable: "Entries",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
