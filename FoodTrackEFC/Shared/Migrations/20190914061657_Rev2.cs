﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodTrackBlazorData.Migrations
{
    public partial class Rev2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Fat_Entries_EntryID",
                table: "Fat");

            migrationBuilder.DropForeignKey(
                name: "FK_Items_Fat_FatID",
                table: "Items");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Fat",
                table: "Fat");

            migrationBuilder.RenameTable(
                name: "Fat",
                newName: "Fats");

            migrationBuilder.RenameIndex(
                name: "IX_Fat_EntryID",
                table: "Fats",
                newName: "IX_Fats_EntryID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Fats",
                table: "Fats",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Fats_Entries_EntryID",
                table: "Fats",
                column: "EntryID",
                principalTable: "Entries",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_Fats_FatID",
                table: "Items",
                column: "FatID",
                principalTable: "Fats",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Fats_Entries_EntryID",
                table: "Fats");

            migrationBuilder.DropForeignKey(
                name: "FK_Items_Fats_FatID",
                table: "Items");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Fats",
                table: "Fats");

            migrationBuilder.RenameTable(
                name: "Fats",
                newName: "Fat");

            migrationBuilder.RenameIndex(
                name: "IX_Fats_EntryID",
                table: "Fat",
                newName: "IX_Fat_EntryID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Fat",
                table: "Fat",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Fat_Entries_EntryID",
                table: "Fat",
                column: "EntryID",
                principalTable: "Entries",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_Fat_FatID",
                table: "Items",
                column: "FatID",
                principalTable: "Fat",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
