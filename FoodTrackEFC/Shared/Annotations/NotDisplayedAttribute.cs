﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodTrackEFC.Shared.Annotations
{
    /// <summary>
    /// Marks a property to not be displayed
    /// </summary>
    public class NotDisplayedAttribute : Attribute
    {
        public override string ToString()
        {
            return "NotDisplayed";
        }

    }
}
