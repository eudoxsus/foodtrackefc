﻿using FoodTrackEFC.Shared.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FoodTrackEFC.Shared.Dtos
{
    /// <summary>
    /// A FoodJournal consists of a collection of FoodEntries associated with a certain date and User
    /// </summary>
    public class FoodJournalDto : IBaseDto
    {
        [NotDisplayed]
        public int ID { get; set; }
        [NotDisplayed]
        public int UserID { get; set; }
        public CalendarEventDto CalendarEvent { get; set; }
        public virtual ICollection<EntryDto> Entries {get; set;}
        public virtual ICollection<RecipeDto> Recipes { get; set; }
    }
}
