﻿using FoodTrackEFC.Shared;
using FoodTrackEFC.Shared.Annotations;
using FoodTrackEFC.Shared.Dtos;
using FoodTrackEFC.Shared.Interfaces;
using FoodTrackEFC.Shared.Interfaces.FormObject;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FoodTrackEFC.Shared.Dtos
{
    public class ItemDto : IBaseDto, IFormObject
    {
        [NotDisplayed]
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        public string Source { get; set; }
        [Required]
        [Range(0, 9999, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int Protein { get; set; }
        [Required]
        [Range(0,9999, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int Calories { get; set; }
        public CarbohydrateDto Carbohydrate { get; set; }
        public FatDto Fat { get; set; }
        public string Unit { get; set; }
        [Required]
        public float BaseSrvSize { get; set; }
        [NotDisplayed]
        public ICollection<ItemRecipeDto> ItemRecipes { get; set; }

        [NotDisplayed]
        public string FormName { get { return "Item"; } }

        [NotDisplayed]
        public string FormNameID { get { return "Item"; } }

        public ItemDto()
        {
            this.Carbohydrate = new CarbohydrateDto(this);
            this.Fat = new FatDto(this);
            this.ItemRecipes = new List<ItemRecipeDto>();
        }

    }
}
