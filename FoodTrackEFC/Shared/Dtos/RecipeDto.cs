﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodTrackEFC.Shared.Dtos
{
    public class RecipeDto : IBaseDto
    {
        public int ID { get; set; }
        public virtual ICollection<ItemRecipeDto> ItemRecipes { get; set; }
    }
}
