﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FoodTrackEFC.Shared.Dtos
{
    public class CalendarEventDto : IBaseDto
    {
        public int ID { get; set; }

        public int UserID { get; set; }

        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Color { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public bool IsFullDay { get; set; }
        public FoodJournalDto FoodJournal { get; set; }
    }
}
