﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FoodTrackEFC.Shared.Dtos
{
    public class EntryDto : IBaseDto
    {
        public int ID { get; set; }
        public ItemDto Item { get; set; }
        // The multiple consumed of this food
        public int QuantityMultiplier;
        [Required]
        public FoodJournalDto FoodJournal;
    }
}
