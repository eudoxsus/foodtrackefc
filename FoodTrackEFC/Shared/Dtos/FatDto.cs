﻿using FoodTrackEFC.Shared.Annotations;
using FoodTrackEFC.Shared.Interfaces.FormObject;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FoodTrackEFC.Shared.Dtos
{
    public class FatDto : IFormObject, IBaseDto
    {
        [NotDisplayed]
        public int ID { get; set; }
        public int Unsaturated { get; set; }
        [Range(0, 9999, ErrorMessage = "{0} invalid. ({1} to {2})")]
        public int Saturated { get; set; }
        public ItemDto Item { get; set; }

        [NotDisplayed]
        public string FormName { get { return "Fat Info"; } }

        [NotDisplayed]
        public string FormNameID { get { return "Fat"; } }

        public FatDto(ItemDto itemDto)
        {
            Item = itemDto;
        }

        public FatDto()
        {
        }
    }
}
