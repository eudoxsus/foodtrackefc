﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodTrackEFC.Shared.Dtos
{
    public interface IBaseDto
    {
        public int ID { get; set; }
    }
}
