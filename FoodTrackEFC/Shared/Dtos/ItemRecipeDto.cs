﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodTrackEFC.Shared.Dtos
{
    public class ItemRecipeDto : IBaseDto
    {
        public int ID { get; set; }
        public int RecipeID { get; set; }
        public RecipeDto Recipe { get; set; }
        public ItemDto Item { get; set; }
        public float ItemSize { get; set; }
    }
}
