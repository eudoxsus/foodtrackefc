﻿using FoodTrackEFC.Shared.Annotations;
using FoodTrackEFC.Shared.Interfaces.FormObject;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FoodTrackEFC.Shared.Dtos
{
    public class CarbohydrateDto : IFormObject, IBaseDto
    {
        [NotDisplayed]
        public int ID { get; set; }
        public ItemDto Item { get; set; }
        [Required]
        [Range(0, 9999, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int Total { get; set; }
        public int Other { get; set; }
        public int Fiber { get; set; }
        public int Sugar { get; set; }
        public int AddedSugar { get; set; }
        [Required]
        [StringLength(9999,MinimumLength = 1,ErrorMessage = "The {0} value must be at least {2} characters and not greater than {1}")]
        public string Unit { get; set; }

        [NotDisplayed]
        public string FormName { get { return "Carb Info"; } }

        [NotDisplayed]
        public string FormNameID { get { return "Carb"; } }

        public CarbohydrateDto(ItemDto itemDto)
        {
            Item = itemDto;
        }

        public CarbohydrateDto()
        {
        }
    }
}
