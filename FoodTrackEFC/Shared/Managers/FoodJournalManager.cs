﻿using FoodTrackBlazorData.Models;
using FoodTrackBlazorData.Repository.Interfaces;
using FoodTrackEFC.Shared.Dtos;
using FoodTrackEFC.Shared.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackEFC.Shared.Managers
{
    public class FoodJournalManager : BaseManager<FoodJournal, FoodJournalDto>, IFoodJournalManager
    {
        private IFoodJournalRepository foodJournalRepository { get; }

        public FoodJournalManager(IFoodJournalRepository foodJournalRepository, IDataMapper dtoMapper) : base(foodJournalRepository, dtoMapper)
        {
            this.foodJournalRepository = foodJournalRepository;
        }
    }
}
