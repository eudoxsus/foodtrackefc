﻿using FoodTrackBlazorData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackEFC.Shared.Managers
{
    /// <summary>
    /// Describes base functionality for a manager class
    /// </summary>
    /// <typeparam name="T">Database Type</typeparam>
    /// <typeparam name="U">Dto Type</typeparam>
    public interface IBaseManager<T, U> where T : BaseEntity where U : class
    {
        Task<U> CreateAsync(U dto);


        Task<U> GetAsync(int id);

        Task<U> UpdateAsync(int id,U dto);

        Task<IEnumerable<U>> GetAsync(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "");

    }
}
