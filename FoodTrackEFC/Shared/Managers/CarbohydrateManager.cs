﻿using FoodTrackBlazorData.Models;
using FoodTrackBlazorData.Repository.Interfaces;
using FoodTrackEFC.Shared.Dtos;
using FoodTrackEFC.Shared.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackEFC.Shared.Managers
{
    public class CarbohydrateManager : BaseManager<Carbohydrate, CarbohydrateDto>, ICarbohydrateManager
    {
        public CarbohydrateManager(ICarbohyrdateRepository carbohyrdateRepository, IDataMapper dtoMapper) : base(carbohyrdateRepository, dtoMapper)
        {

        }
    }
}
