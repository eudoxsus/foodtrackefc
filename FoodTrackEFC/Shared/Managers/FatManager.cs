﻿using FoodTrackBlazorData.Models;
using FoodTrackBlazorData.Repository.Interfaces;
using FoodTrackEFC.Shared.Dtos;
using FoodTrackEFC.Shared.Mappers;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodTrackEFC.Shared.Managers
{
    public class FatManager : BaseManager<Fat,FatDto>, IFatManager
    {

        public FatManager(IFatRepository fatRepository, IDataMapper dtoMapper) : base(fatRepository, dtoMapper)
        {

        }
    }
}
