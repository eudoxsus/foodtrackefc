﻿using FoodTrackBlazorData.Models;
using FoodTrackBlazorData.Repository.Interfaces;
using FoodTrackEFC.Shared.Dtos;
using FoodTrackEFC.Shared.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackEFC.Shared.Managers
{
    /// <summary>
    /// Base Abstract Class for a Manager class to extend
    /// </summary>
    /// <typeparam name="T">Database Type</typeparam>
    /// <typeparam name="U">Dto Type</typeparam>
    public abstract class BaseManager<T,U> : IBaseManager<T,U> where T : BaseEntity, new() where U : class, IBaseDto, new()
    {
        protected IBaseRepository<T> baseRepository { get; }
        protected IDataMapper dtoDataMapper { get; }

        public virtual async Task<U> CreateAsync(U dto)
        {
            //The dto should not have an ID set here
            var dbObj = dtoDataMapper.Convert<T, U>(dto);
            var createdObj = await baseRepository.CreateAsync(dbObj);
            return dtoDataMapper.Convert<U, T>(createdObj);
        }

        /// <summary>
        /// Update a db item by id
        /// </summary>
        /// <param name="id">ID of item to update in db</param>
        /// <param name="dto"></param>
        /// <returns></returns>
        public virtual async Task<U> UpdateAsync(int id,U dto)
        {
            ///TODO Check that the DTO and given ID are the same
            var dbObj = await baseRepository.GetAsync(dto.ID);
            if (dbObj == null)
            {
                throw new ArgumentException($"Dto with ID of {dto.ID} could not be retrieved from database");
            }
            else
            {
                // Convert dto to db object
                UpdateDbObj(dto,dbObj);
                return dto;
            }
        }

        /// <summary>
        /// Update a bunch of items in database matching the filter's spec
        /// </summary>
        /// <param name="filter">Filter expression to match objects</param>
        /// <param name="dto">The source dto to update items in the database</param>
        /// <returns></returns>
        public virtual async Task<U> UpdateAsync(Expression<Func<T, bool>> filter, U dto)
        {
            var dbObjs = await baseRepository.GetAsync(filter);
            foreach (T dbObj in dbObjs)
            {
                UpdateDbObj(dto, dbObj);
            }
            return dto;
        }

        private async void UpdateDbObj(U dto,T dbObj)
        {
            // Convert dto to db object
            dtoDataMapper.Map(dto, dbObj);
            baseRepository.SetEntryModified(dbObj);
            await baseRepository.SaveAsync();
        }

        public virtual async Task<U> GetAsync(int id)
        {
            var dbObj = await baseRepository.GetAsync(id);
            return dbObj != null ? dtoDataMapper.Convert<U, T>(dbObj) : null;
        }

        public virtual async Task<IEnumerable<U>> GetAsync(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "")
        {
            var dbRes = await baseRepository.GetAsync(filter, orderBy, includeProperties);
            List<U> dtos = new List<U>();
            foreach(var res in dbRes)
            {
                dtos.Add(dtoDataMapper.Convert<U, T>(res));
            }
            return dtos;

        }

        public BaseManager(IBaseRepository<T> baseRepository, IDataMapper dtoDataMapper)
        {
            this.baseRepository = baseRepository;
            this.dtoDataMapper = dtoDataMapper;
        }

    }
}
