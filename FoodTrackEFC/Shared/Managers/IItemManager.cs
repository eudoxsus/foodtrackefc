﻿using FoodTrackBlazorData.Models;
using FoodTrackEFC.Shared.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodTrackEFC.Shared.Managers
{
    public interface IItemManager : IBaseManager<Item,ItemDto>
    {
    }
}
