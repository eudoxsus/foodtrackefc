﻿using FoodTrackBlazorData.Models;
using FoodTrackBlazorData.Repository.Interfaces;
using FoodTrackEFC.Shared.Dtos;
using FoodTrackEFC.Shared.Mappers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackEFC.Shared.Managers
{
    public class ItemManager : BaseManager<Item,ItemDto>, IItemManager
    {
        IFatManager FatManager { get; }
        ICarbohydrateManager CarboHydrateManager { get; }

        public ItemManager(IItemRepository itemRepository, IFatManager fatManager, ICarbohydrateManager carboHydrateManager, IDataMapper dtoMapper) : base(itemRepository,dtoMapper)
        {
            FatManager = fatManager;
            CarboHydrateManager = carboHydrateManager;
        }
    }
}
