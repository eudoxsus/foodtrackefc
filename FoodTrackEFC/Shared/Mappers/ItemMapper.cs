﻿
using FoodTrackBlazorData.Interfaces;
using FoodTrackBlazorData.Models;
using FoodTrackEFC.Shared.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodTrackEFC.Shared.Mappers
{
    public class ItemMapper : IDataConverter<Item,ItemDto>
    {
        public IDataMapper DtoDataMapper { get; }

        public void DtoToData(ItemDto dto, in Item item)
        {
            item.BaseSrvSize = dto.BaseSrvSize;
            item.Calories = dto.Calories;
            item.Carbohydrate = DtoDataMapper.Convert<Carbohydrate, CarbohydrateDto>(dto.Carbohydrate);
            item.Fat = DtoDataMapper.Convert<Fat, FatDto>(dto.Fat);
            //item.ID = dto.ID;
            foreach (var itemRec in dto.ItemRecipes ?? new List<ItemRecipeDto>())
            {
                item.ItemRecipes.Add(DtoDataMapper.Convert<ItemRecipe, ItemRecipeDto>(itemRec));
            }
            item.Name = dto.Name;
            item.Protein = dto.Protein;
            item.Source = dto.Source;
            item.Unit = dto.Unit;
        }

        public void DataToDto(Item item, in ItemDto dto)
        {
            dto.BaseSrvSize = item.BaseSrvSize;
            dto.Calories = item.Calories;
            dto.Carbohydrate = DtoDataMapper.Convert<CarbohydrateDto, Carbohydrate>(item.Carbohydrate);
            dto.Fat = DtoDataMapper.Convert<FatDto, Fat>(item.Fat);
            dto.ID = item.ID;
            foreach (var itemRec in item.ItemRecipes ?? new List<ItemRecipe>())
            {
                dto.ItemRecipes.Add(DtoDataMapper.Convert<ItemRecipeDto, ItemRecipe>(itemRec));
            }
            dto.Name = item.Name;
            dto.Protein = item.Protein;
            dto.Source = item.Source;
            dto.Unit = item.Unit;
        }

        public ItemMapper(IDataMapper IDtoDataMapper)
        {
            this.DtoDataMapper = IDtoDataMapper;
        }
    }
}
