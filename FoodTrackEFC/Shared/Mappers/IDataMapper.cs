﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodTrackEFC.Shared.Mappers
{
    /// <summary>
    /// Interface that will map a DTO object to a Database object and back again
    /// </summary>
    public interface IDataMapper
    {
        /// <summary>
        /// Returns a new object of the opposite type that is given
        /// </summary>
        /// <typeparam name="T">To Object type</typeparam>
        /// <typeparam name="U">From Object type</typeparam>
        /// <param name="dataObj">The object to be converted</param>
        /// <returns></returns>
        T Convert<T, U>(U dataObj) where T : new();

        /// <summary>
        /// Maps values from one object to another without creating a new instance
        /// </summary>
        /// <typeparam name="T">To Object type</typeparam>
        /// <typeparam name="U">From Object type</typeparam>
        /// <param name="toObj">The object to copy values to</param>
        /// <param name="fromObj">The object to copy values from</param>
        void Map<T, U>(U toObj,T fromObj);

        /// <summary>
        /// Adds a DataConverter to be used by the DataMapper
        /// </summary>
        /// <typeparam name="T">Database Object type</typeparam>
        /// <typeparam name="U">Dto Object type</typeparam>
        /// <param name="converter"></param>
        void AddConverter<T, U>(IDataConverter<T, U> converter);

        IReadOnlyDictionary<Type,object> PostedObjects { get; }
    }
}
