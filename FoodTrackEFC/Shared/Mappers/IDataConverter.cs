﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodTrackEFC.Shared.Mappers
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T">Database Type</typeparam>
    /// <typeparam name="U">Dto Type</typeparam>
    public interface IDataConverter<T,U>
    {
        void DataToDto(T data, in U dto);

        void DtoToData(U dto, in T data);
    }
}
