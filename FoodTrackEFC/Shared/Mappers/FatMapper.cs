﻿using FoodTrackBlazorData.Models;
using FoodTrackEFC.Shared.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodTrackEFC.Shared.Mappers
{
    public class FatMapper : IDataConverter<Fat, FatDto>
    {

        public IDataMapper DtoDataMapper { get; }

        public void DataToDto(Fat data, in FatDto dto)
        {
            dto.Item = DtoDataMapper.Convert<ItemDto, Item>(data.Item);
            dto.Saturated = data.Saturated;
            dto.ID = data.ID;
            dto.Unsaturated = data.Unsaturated;
        }

        public void DtoToData(FatDto dto, in Fat data)
        {
            data.Item = DtoDataMapper.Convert<Item, ItemDto>(dto.Item);
            data.Saturated = dto.Saturated;
            //data.ID = dto.ID;
            data.Unsaturated = dto.Unsaturated;
        }


        public FatMapper(IDataMapper IDtoDataMapper)
        {
            this.DtoDataMapper = IDtoDataMapper;
        }

    }
}
