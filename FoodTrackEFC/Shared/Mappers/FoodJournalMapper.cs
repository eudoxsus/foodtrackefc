﻿using FoodTrackBlazorData.Models;
using FoodTrackEFC.Shared.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodTrackEFC.Shared.Mappers
{
    public class FoodJournalMapper : IDataConverter<FoodJournal, FoodJournalDto>
    {
        private IDataMapper DtoDataMapper { get; }

        public void DataToDto(FoodJournal data, in FoodJournalDto dto)
        {
            dto.CalendarEvent = DtoDataMapper.Convert<CalendarEventDto, CalendarEvent>(data.CalendarEvent);
            dto.Entries.Clear();
            foreach (Entry entry in data.Entries)
            {
                EntryDto entryDto = DtoDataMapper.Convert<EntryDto, Entry>(entry);
                dto.Entries.Add(entryDto);
            }
            dto.Recipes.Clear();
            foreach (Recipe recipe in data.Recipes)
            {
                RecipeDto recipeDto = DtoDataMapper.Convert<RecipeDto, Recipe>(recipe);
                dto.Recipes.Add(recipeDto);
            }
        }

        public void DtoToData(FoodJournalDto dto, in FoodJournal data)
        {
            data.CalendarEvent = DtoDataMapper.Convert<CalendarEvent, CalendarEventDto>(dto.CalendarEvent);
            data.Entries.Clear();
            foreach(EntryDto entryDto in dto.Entries)
            {
                Entry entry = DtoDataMapper.Convert<Entry, EntryDto>(entryDto);
                data.Entries.Add(entry);
            }
            data.Recipes.Clear();
            foreach (RecipeDto recipeDto in dto.Recipes)
            {
                Recipe recipe = DtoDataMapper.Convert<Recipe, RecipeDto>(recipeDto);
                data.Recipes.Add(recipe);
            }
        }

        public FoodJournalMapper(IDataMapper IDtoDataMapper)
        {
            this.DtoDataMapper = IDtoDataMapper;
        }
    }
}
