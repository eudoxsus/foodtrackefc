﻿using FoodTrackBlazorData.Models;
using FoodTrackEFC.Shared.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodTrackEFC.Shared.Mappers
{
    public class CarbohydrateMapper : IDataConverter<Carbohydrate, CarbohydrateDto>
    {

        public IDataMapper DtoDataMapper { get; }

        public void DataToDto(Carbohydrate data, in CarbohydrateDto dto)
        {
            dto.Fiber = data.Fiber;
            dto.AddedSugar = data.AddedSugar;
            dto.ID = data.ID;
            dto.Item = DtoDataMapper.Convert<ItemDto, Item>(data.Item);
            dto.Other = data.Other;
            dto.Sugar = data.Sugar;
            dto.Total = data.Total;
            dto.Unit = data.Unit;
        }

        public void DtoToData(CarbohydrateDto dto, in Carbohydrate data)
        {
            data.Fiber = dto.Fiber;
            data.AddedSugar = dto.AddedSugar;
            //data.ID = dto.ID;
            data.Item = DtoDataMapper.Convert<Item, ItemDto>(dto.Item);
            data.Other = dto.Other;
            data.Sugar = dto.Sugar;
            data.Total = dto.Total;
            data.Unit = dto.Unit;
        }

        public CarbohydrateMapper(IDataMapper IDtoDataMapper)
        {
            this.DtoDataMapper = IDtoDataMapper;
        }
    }
}
