﻿using FoodTrackBlazorData.Interfaces;
using FoodTrackBlazorData.Models;
using FoodTrackEFC.Shared.Dtos;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace FoodTrackEFC.Shared.Mappers
{
    public class DataMapper : IDataMapper
    {

        /// <summary>
        /// This Dictionary 
        /// </summary>
        private Dictionary<Type, object> Converters { get; }

        public IReadOnlyDictionary<Type, object> PostedObjects { get {
                return _postedObjects;
        } }

        private ConcurrentDictionary<Type, object> _postedObjects;

        private delegate void ConvertFunc<T, U>(U dataobj,in T outObj);

        public T Convert<T, U>(U ConvertObj) where T : new()
        {
            Type fromType = typeof(U);
            Type toType = typeof(T);
            object value;
            object func;
            T result;

            if (_postedObjects.TryGetValue(toType, out value))
            {
                return (T)value;
            }


            if (Converters.TryGetValue(typeof(T),out func))
            {
                result = new T();
                ConvertFunc<T, U> convertFunc = func as ConvertFunc<T, U>;
                if (convertFunc == null)
                {
                    throw new ArgumentException($"Object stored was not a Converter {typeof(T).ToString()}");
                }
                _postedObjects.TryAdd(toType, result);
                convertFunc(ConvertObj,result);
                _postedObjects.TryRemove(toType, out value);
                return result;
            }
            throw new ArgumentException($"No mapper found for type of {typeof(T).ToString()}");

        }
        
        public void AddConverter<T,U>(IDataConverter<T,U> converter)
        {
            ConvertFunc<T, U> dtoConverter = converter.DtoToData;
            ConvertFunc<U, T> dataConverter = converter.DataToDto;
            Converters.Add(typeof(T), dtoConverter);
            Converters.Add(typeof(U), dataConverter);
        }

        public void Map<T, U>(U fromObj, T toObj)
        {
            object func;

            if (Converters.TryGetValue(typeof(T), out func))
            {
                ConvertFunc<T, U> convertFunc = func as ConvertFunc<T, U>;
                if (convertFunc == null)
                {
                    throw new ArgumentException($"Object stored was not a Converter {typeof(T).ToString()}");
                }
                convertFunc(fromObj, toObj);
            }
            else
            {
                throw new ArgumentException($"No mapper found for type of {typeof(T).ToString()}");
            }
        }

        public DataMapper()
        {
            Converters = new Dictionary<Type, object>();
            _postedObjects = new ConcurrentDictionary<Type, object>();
            ItemMapper itemMapper = new ItemMapper(this);
            CarbohydrateMapper carbohydrateMapper = new CarbohydrateMapper(this);
            FatMapper fatMapper = new FatMapper(this);
            FoodJournalMapper foodJournalMapper = new FoodJournalMapper(this);
            AddConverter(fatMapper);
            AddConverter(itemMapper);
            AddConverter(carbohydrateMapper);
            AddConverter(foodJournalMapper);
        }

    }
}
