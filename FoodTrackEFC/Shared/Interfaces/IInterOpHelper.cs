﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodTrackEFC.Shared.Interfaces
{
    public interface IInterOpHelper
    {
        string TestInter(string test);
        ValueTask<object> CreateJSRef();
    }
}
