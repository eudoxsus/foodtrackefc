﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodTrackEFC.Shared.Interfaces.FormObject
{
    interface IIntFormObj
    {
        int Property { get; }
    }
}
