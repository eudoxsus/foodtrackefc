﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodTrackEFC.Shared.Interfaces.FormObject
{
    public interface INamedFormObject
    {
        string Name { get; }

        string ID { get; }
    }
}
