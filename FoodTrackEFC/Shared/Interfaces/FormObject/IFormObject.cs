﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodTrackEFC.Shared.Interfaces.FormObject
{
    /// <summary>
    /// Objects that can be displayed in a form, done through reflection.
    /// </summary>
    public interface IFormObject
    { 

        /// <summary>
        /// The name to display of this object in the form
        /// </summary>
        string FormName { get; }

        /// <summary>
        /// The unique name of this FormObject
        /// </summary>
        string FormNameID { get; }
    }
}
