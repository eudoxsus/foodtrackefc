﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Microsoft.AspNetCore.Components.Forms;

namespace FoodTrackEFC.Shared.Interfaces.FormObject
{
    public interface IFormObjectProperty<T> : INamedFormObject
    {
        T Value { get; set; }
        Action<T> ValueChanged { get; }
        Expression<Func<T>> ValueExpression { get; }
        EditContext EditContext { get; }
    }
}
