﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodTrackEFC.Shared.Interfaces.FormObject
{
    public interface IStringFormObj : INamedFormObject
    {
        string Property { get; }
    }
}
