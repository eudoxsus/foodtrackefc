﻿using FoodTrackBlazorData.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodTrackBlazorData.Contexts
{
    public class FoodTrackContext : DbContext
    {
        public FoodTrackContext(DbContextOptions<FoodTrackContext> options) : base(options)
        { }

        public DbSet<CalendarEvent> CalendarEvents { get; set; }
        public DbSet<Carbohydrate> Carbohydrates { get; set; }
        public DbSet<Entry> Entries { get; set; }
        public DbSet<Fat> Fats { get; set; }
        public DbSet<FoodJournal> FoodJournals { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<ItemRecipe> ItemRecipes { get; set; }
        public DbSet<Recipe> Recipes { get; set; }
    }
}
