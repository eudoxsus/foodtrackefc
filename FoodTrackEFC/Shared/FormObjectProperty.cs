﻿using FoodTrackEFC.Shared.Interfaces.FormObject;
using Microsoft.AspNetCore.Components.Forms;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace FoodTrackEFC.Shared
{
    public class FormObjectProperty<T> : IFormObjectProperty<T>
    {
        public T Value { get; set; }

        public string Name { get; }

        public string ID { get; }

        public Action<T> ValueChanged { get; }

        public Expression<Func<T>> ValueExpression  { get; }

        public EditContext EditContext { get; }

        public FormObjectProperty(PropertyInfo propInf,IFormObject ifo,EditContext editContext,OrderedSet<object> parentSet)
        {
            this.EditContext = editContext;
            StringBuilder stringBuilder = new StringBuilder();
            Value = (T)propInf.GetValue(ifo);
            Name = propInf.Name;
            // Create the ID by appending the parents together
            foreach(IFormObject obj in parentSet)
            {
                stringBuilder.Append(obj.FormNameID);
                stringBuilder.Append("/");
            }
            ID = stringBuilder.ToString();
            ValueExpression = () => Value;
            // Specify the ValueChanged event handler to set the property info value
            ValueChanged = (T val) =>
            { 
                propInf.SetValue(ifo, val);
                EditContext.NotifyFieldChanged(editContext.Field(Name));
            };
            
        }
    }
}
