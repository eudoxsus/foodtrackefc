﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodTrackBlazorData.Models
{
    public class User
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public FoodJournal FoodJournal { get; set; }
    }
}
