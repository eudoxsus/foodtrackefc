﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FoodTrackBlazorData.Models
{
    public class Entry
    {
        public int ID { get; set; }
        [Required]
        public DateTime Date { get; set; }
        public Item Item { get; set; }
        // The multiple consumed of this food
        public int QuantityMultiplier;
        [Required]
        public FoodJournal FoodJournal;
    }
}
