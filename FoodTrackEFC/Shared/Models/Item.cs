﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodTrackBlazorData.Models
{
    public class Item
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Source { get; set; }
        public int Protein { get; set; }
        public int Calories { get; set; }
        public Carbohydrate Carbohydrates { get; set; }
        public Fat Fat { get; set; }
        public string Unit { get; set; }
        public float BaseSrvSize { get; set; }
        public virtual ICollection<ItemRecipe> ItemRecipes { get; set; }
    }
}
