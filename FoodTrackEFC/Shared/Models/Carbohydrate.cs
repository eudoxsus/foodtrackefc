﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FoodTrackBlazorData.Models
{
    public class Carbohydrate
    {
        public int ID { get; set; }
        public Item Item { get; set; }
        public int Total { get; set; }
        public int Other { get; set; }
        public int Fiber { get; set; }
        public int Sugar { get; set; }
        public int AddedSugar { get; set; }
        [Required]
        public string Unit { get; set; }
    }
}
