﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FoodTrackBlazorData.Models
{
    public class Fat
    {
        public int ID { get; set; }
        public int Unsaturated { get; set; }
        public int Saturated { get; set; }
        public Entry Entry { get; set; }
    }
}
