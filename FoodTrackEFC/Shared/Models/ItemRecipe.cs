﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodTrackBlazorData.Models
{
    public class ItemRecipe
    {
        public int ID { get; set; }
        public int RecipeID { get; set; }
        public Recipe Recipe { get; set; }
        public Item Item { get; set; }
        public float ItemSize { get; set; }
    }
}
