﻿using FoodTrackEFC.Shared.Dtos;
using FoodTrackEFC.Shared.Managers;
using FoodTrackEFC.Shared.Mappers;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FoodTrackBlazorDataTest.Repository
{
    public class DBTests : RepositoryTest
    {

        [Fact]
        public async Task CreateItemTest()
        {
            var options = BuildInMemoryDatabase("Create_Item");

            var newItem = new FoodTrackBlazorData.Models.Item
            {
                Calories = 100,
                Name = "Egg",
                ID = 1
            };


            using (var context = new FoodTrackBlazorData.Contexts.FoodTrackContext(options))
            {
                var itemRepo = new FoodTrackBlazorData.Repository.Impl.ItemRepository(context);
                await itemRepo.CreateAsync(newItem);
            }

            Assert.NotNull(newItem);
            Assert.Equal(1, newItem.ID);

            newItem = null;

            using (var context = new FoodTrackBlazorData.Contexts.FoodTrackContext(options))
            {
                var itemRepo = new FoodTrackBlazorData.Repository.Impl.ItemRepository(context);
                Expression<Func<FoodTrackBlazorData.Models.Item, bool>> filterById = x => x.ID == 1;
                var res = await itemRepo.GetAsync(filterById);
                var enumer = res.GetEnumerator();
                enumer.MoveNext();
                newItem = enumer.Current;

                Assert.NotNull(newItem);
                Assert.Equal(1, newItem.ID);
            }



        }

        [Fact]
        public async Task UpdateItemTest()
        {
            var options = BuildInMemoryDatabase("Update_Item");

            var newFat = new FatDto
            {
                ID = 1,
                Saturated = 1,
                Unsaturated = 1
            };

            var newItem = new ItemDto
            {
                Calories = 100,
                Name = "Egg",
                ID = 1,
                Fat = newFat
            };


            using (var context = new FoodTrackBlazorData.Contexts.FoodTrackContext(options))
            {
                var itemRepo = new FoodTrackBlazorData.Repository.Impl.ItemRepository(context);
                var fatRepo = new FoodTrackBlazorData.Repository.Impl.FatRepository(context);
                var carbRepo = new FoodTrackBlazorData.Repository.Impl.CarbohydrateRepository(context);
                var dataMapper = new DataMapper();
                var fatMan = new FatManager(fatRepo, dataMapper);
                var carbMan = new CarbohydrateManager(carbRepo, dataMapper);
                var itemMan = new ItemManager(itemRepo, fatMan, carbMan, dataMapper);
                await itemMan.CreateAsync(newItem);
            

                Assert.NotNull(newItem);
                Assert.Equal(1, newItem.ID);

                newItem = null;
                
                Expression<Func<FoodTrackBlazorData.Models.Item, bool>> filterById = x => x.ID == 1;
                var res = await itemMan.GetAsync(filterById);
                var enumer = res.GetEnumerator();
                enumer.MoveNext();
                newItem = enumer.Current;

                Assert.NotNull(newItem);
                Assert.Equal(1, newItem.ID);

                newItem.Calories = 200;

                await itemMan.UpdateAsync(newItem.ID, newItem);
                newItem = null;

                res = await itemMan.GetAsync(filterById);
                enumer = res.GetEnumerator();
                enumer.MoveNext();
                newItem = enumer.Current;

                Assert.NotNull(newItem);
                Assert.Equal(200, newItem.Calories);
            }



        }
    }
}
