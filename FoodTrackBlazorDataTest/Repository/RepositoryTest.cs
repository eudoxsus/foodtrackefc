﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodTrackBlazorDataTest.Repository
{
    public abstract class RepositoryTest
    {
        protected DbContextOptions<FoodTrackBlazorData.Contexts.FoodTrackContext> BuildInMemoryDatabase(string databaseName)
        {
            var options = new DbContextOptionsBuilder<FoodTrackBlazorData.Contexts.FoodTrackContext>()
                .UseInMemoryDatabase(databaseName: databaseName)
                .Options;
            return options;
        }
    }
}
