﻿using FoodTrackEFC.Shared.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using Xunit;
using static FoodTrackEFC.Client.Static.CustomHttpClientJsonExtensions;

namespace FoodTrackBlazorDataTest.Repository
{
    public class ApiTests
    {
        [Fact]
        public void ItemSerializeTest()
        {
            var newItem = new ItemDto
            {
                Calories = 100,
                Name = "Steak"
            };

            ItemDto deserialized;

            var requestJson = JsonSerializer.Serialize(newItem, JsonSerializerOptionsProvider.Options);
            deserialized = JsonSerializer.Deserialize<ItemDto>(requestJson, JsonSerializerOptionsProvider.Options);
            
        }
    }
}
